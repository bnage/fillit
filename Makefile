# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bnage <bnage@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/04 15:36:29 by bnage             #+#    #+#              #
#    Updated: 2019/02/15 17:50:56 by bnage            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRC = $(wildcard src/*.c)
OBJJ = $(SRC:src/*.c=.o)
OBJ = $(patsubst src/%.c, %.o, $(SRC))
NAME = fillit
INCLUDES = includes
LIBFOLDER = libft
LIB = libft/libft.a

debug: $(LIB)
	gcc -g -c $(SRC) -I $(LIBFOLDER) -I $(INCLUDES)
	gcc -g $(OBJ) -o $(NAME) -L $(LIBFOLDER) -lft
	./$(NAME)
all: $(NAME)
%.o: src/%.c
	gcc -Wall -Wextra -Werror -c $< -I $(LIBFOLDER) -I $(INCLUDES)
$(LIB):
	make -C $(LIBFOLDER)
$(NAME): $(OBJ) $(LIB)
	gcc -Wall -Wextra -Werror $(OBJ) -o $(NAME) -L $(LIBFOLDER) -lft
clean:
	make -C $(LIBFOLDER) clean
	rm -f *.o
fclean:
	make -C $(LIBFOLDER) fclean
	rm -f *.o
	rm -f $(NAME)
tfclean: clean
	rm -f $(NAME)
re: fclean fclean all
