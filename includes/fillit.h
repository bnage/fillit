/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <bnage@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/15 14:51:50 by bnage             #+#    #+#             */
/*   Updated: 2019/02/15 18:09:44 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H
# include <stdint.h>

typedef struct s_figure	t_figure;
typedef struct s_map	t_map;

struct	s_figure
{
	uint64_t	bits_value;
	int			width;
	int			height;

	int			pos_x;
	int			pos_y;
	t_figure	*prev;
};

struct	s_map
{
	uint16_t	map[16];
	int			size;
};

void	to_figure(char fig[16], t_figure *x);
int		is_default(t_figure *x);
void	set_default(t_figure *x);

int		read_file(char *file, t_figure x[26]);

void	dfs(int i, int x[4], int st[4]);
int		nbour_points(int xy, int xy2, int n);
int		reverse_xy(int xy, int n);
int		sqrtfloor(int n);
int		sqrtceil(int n);

int		fillit(t_map *map, t_figure x[26]);

#endif
