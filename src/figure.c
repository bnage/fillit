/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   figure.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <bnage@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/14 18:20:58 by bnage             #+#    #+#             */
/*   Updated: 2019/02/15 18:13:59 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fillit.h>
#include <string.h>

static void		normalize(char fig[16])
{
	int i;
	int j;
	int k;
	int r;

	j = -1;
	i = -1;
	while (++i < 16)
		if (fig[i] == '#')
			break ;
	while (++j < 16)
		if (fig[reverse_xy(j, 4)] == '#')
			break ;
	i = (i % 16) / 4 - 1;
	j = (j % 16) / 4 - 1;
	if ((r = i + 1) != 0 || j != -1)
		while (++i < 4 && (k = j + 1) != -3)
		{
			while (++j < 4)
			{
				fig[4 * (i - r) + j - k] = fig[4 * i + j];
				fig[4 * i + j] = '.';
			}
			j = k - 1;
		}
}

static int		have_pairs(int x[4])
{
	int st[4];
	int i;

	i = -1;
	while (++i < 4)
		st[i] = 0;
	dfs(0, x, st);
	i = -1;
	while (++i < 4)
		if (st[i] != 1)
			return (0);
	return (1);
}

void			to_figure(char fig[16], t_figure *x)
{
	int pos[4];
	int i;
	int j;

	i = 0;
	j = 0;
	set_default(x);
	normalize(fig);
	while (i < 16)
	{
		if (fig[i] == '#' && j < 4)
		{
			pos[j] = i;
			x->width = i % 4 + 1 > x->width ? i % 4 + 1 : x->width;
			x->height = i / 4 + 1 > x->height ? i / 4 + 1 : x->height;
		}
		j += fig[i++] == '#' ? 1 : 0;
	}
	i = -1;
	if (j == 4 && have_pairs(pos))
		while (++i < 4)
			x->bits_value |= 1L << (16 * (pos[i] / 4) + (pos[i] % 4));
}

int				is_default(t_figure *x)
{
	if (x == NULL)
		return (1);
	return (x->bits_value == 0ULL ? 1 : 0);
}

void			set_default(t_figure *x)
{
	if (x == NULL)
		return ;
	x->prev = NULL;
	x->bits_value = 0;
	x->height = 1;
	x->width = 1;
}
