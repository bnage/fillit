/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <bnage@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/15 16:17:03 by bnage             #+#    #+#             */
/*   Updated: 2019/02/15 18:14:27 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>
#include <fillit.h>

static int	read_tet(int fd, t_figure *x)
{
	char	fig[21];
	char	fig2[16];
	int		i;
	int		j;
	int		z;

	i = read(fd, fig, 21);
	if (i < 20 || fig[i - 1] != '\n')
		return (-1);
	j = -1;
	z = 0;
	while (++j < 20)
	{
		if (j % 5 == 4)
		{
			if (fig[j] != '\n')
				return (-1);
		}
		else if (fig[j] != '.' && fig[j] != '#')
			return (-1);
		else
			fig2[z++] = fig[j];
	}
	to_figure(fig2, x);
	return (is_default(x) ? -1 : i - 20);
}

int			read_file(char *file, t_figure x[26])
{
	int fd;
	int i;
	int gnl;

	i = 0;
	fd = open(file, O_RDONLY);
	gnl = fd < 0 ? 0 : 1;
	while (gnl == 1 && i < 26)
		gnl = read_tet(fd, &x[i++]);
	i = gnl != 0 ? 0 : i;
	if (i != 26)
		set_default(&x[i]);
	if (fd > 0)
		close(fd);
	fd = 0;
	while (++fd < i && (gnl = fd))
		while (--gnl >= 0)
			if (x[fd].bits_value == x[gnl].bits_value)
			{
				x[fd].prev = x + gnl;
				break ;
			}
	return (i);
}
