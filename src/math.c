/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   math.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <bnage@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/14 18:33:29 by bnage             #+#    #+#             */
/*   Updated: 2019/02/15 17:38:54 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		reverse_xy(int xy, int n)
{
	return ((xy % n) * n + (xy / n));
}

int		nbour_points(int xy, int xy2, int n)
{
	int x;

	if (xy / n == xy2 / n)
		x = xy % n - xy2 % n;
	else if (xy % n == xy2 % n)
		x = xy / n - xy2 / n;
	else
		return (0);
	x = x < 0 ? -x : x;
	return (x == 1 ? 1 : 0);
}

void	dfs(int i, int x[4], int st[4])
{
	int j;

	if (st[i])
		return ;
	st[i] = 1;
	j = -1;
	while (++j < 4)
	{
		if (nbour_points(x[i], x[j], 4))
			dfs(j, x, st);
	}
}

int		sqrtceil(int n)
{
	int a;
	int b;
	int mid;

	a = 0;
	b = n > 46340 ? 46340 : n;
	if (n <= 0)
		return (n);
	if (b * b == n)
		return (b);
	while (a + 1 != b)
	{
		mid = (a + b) / 2;
		if (mid * mid < n)
			a = mid;
		else if (mid * mid > n)
			b = mid;
		else
			return (mid);
	}
	return (b);
}
