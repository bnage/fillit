/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   think.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <bnage@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/15 14:48:25 by bnage             #+#    #+#             */
/*   Updated: 2019/02/15 18:24:11 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fillit.h>

static void		xor_figure(t_map *map, t_figure f)
{
	*(uint64_t *)(map->map + f.pos_y) ^= f.bits_value << (f.pos_x);
}

static int		try_place(t_map *map, t_figure f)
{
	if (((*(uint64_t *)(map->map + f.pos_y)) & (f.bits_value << (f.pos_x))))
		return (0);
	xor_figure(map, f);
	return (1);
}

static int		place(t_map *map, t_figure fig[26], int i)
{
	int y;
	int x;

	if (i == 26 || is_default(&fig[i]))
		return (1);
	y = fig[i].prev ? (*(fig[i].prev)).pos_y - 1 : -1;
	while (++y < map->size - fig[i].height + 1)
	{
		fig[i].pos_y = y;
		x = -1;
		if (fig[i].prev)
			x = y == (*(fig[i].prev)).pos_y ? (*(fig[i].prev)).pos_x - 1 : x;
		while (++x < map->size - fig[i].width + 1)
		{
			fig[i].pos_x = x;
			if (try_place(map, fig[i]))
			{
				if (place(map, fig, i + 1))
					return (1);
				xor_figure(map, fig[i]);
			}
		}
	}
	return (0);
}

int				fillit(t_map *map, t_figure x[26])
{
	int i;
	int t;
	int max;

	i = 0;
	max = 0;
	while (i < 26 && !is_default(&x[i]))
	{
		t = x[i].width > x[i].height ? x[i].width : x[i].height;
		max = t > max ? t : max;
		i++;
	}
	t = sqrtceil(4 * i);
	map->size = max > t ? max : t;
	while (map->size <= 16 && !place(map, x, 0))
		map->size++;
	return (map->size <= 16 ? 1 : 0);
}
