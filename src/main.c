/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <bnage@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/15 16:30:38 by bnage             #+#    #+#             */
/*   Updated: 2019/02/15 17:40:14 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <fillit.h>

static void	draw_figure(t_figure fig, char (*map)[256], int size, char c)
{
	int			i;
	uint64_t	j;

	i = 0;
	j = 1;
	while (i < 64)
	{
		if ((fig.bits_value & j))
			(*map)[size * (fig.pos_y + i / 16) + fig.pos_x + i % 16] = c;
		j = j << 1;
		i++;
	}
}

static void	draw_map(t_figure fig[26], int size)
{
	char	map[256];
	int		i;

	i = 0;
	while (i < 256)
		map[i++] = '.';
	i = 0;
	while (i < 26 && !is_default(&fig[i]))
	{
		draw_figure(fig[i], &map, size, 'A' + i);
		i++;
	}
	i = 0;
	while (i < size * size)
	{
		ft_putchar(map[i]);
		if (i++ % size == size - 1 && i != 1)
			ft_putchar('\n');
	}
}

int			main(int ac, char **av)
{
	t_figure	x[26];
	t_map		map;
	int			i;

	i = 0;
	if (ac != 2)
	{
		ft_putendl("usage: ./fillit sample.fillit");
		return (0);
	}
	read_file(av[1], x);
	if (is_default(&x[0]))
	{
		ft_putendl("error");
		return (1);
	}
	while (i < 16)
		map.map[i++] = 0;
	fillit(&map, x);
	draw_map(x, map.size);
	return (0);
}
