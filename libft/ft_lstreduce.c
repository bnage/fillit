/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstreduce.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/08 19:33:06 by bnage             #+#    #+#             */
/*   Updated: 2018/12/08 22:32:03 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstreduce(t_list *el, void *res, void (*f)(void *, void *, size_t))
{
	if (el == NULL || f == NULL || *f == NULL)
		return ;
	while (el)
	{
		f(res, el->content, el->content_size);
		el = el->next;
	}
}
