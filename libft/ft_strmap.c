/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/04 20:08:54 by bnage             #+#    #+#             */
/*   Updated: 2018/12/08 20:47:14 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*x;
	size_t	i;

	if (s == NULL || f == NULL || *f == NULL)
		return (NULL);
	i = ft_strlen(s);
	x = (char *)malloc(i + 1);
	if (x == NULL)
		return (NULL);
	x[i] = '\0';
	i = 0;
	while (s[i])
	{
		x[i] = f(s[i]);
		i++;
	}
	return (x);
}
