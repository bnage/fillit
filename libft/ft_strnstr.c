/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 17:29:55 by bnage             #+#    #+#             */
/*   Updated: 2018/12/07 17:48:21 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *hs, const char *ndl, size_t len)
{
	size_t i;
	size_t j;

	if (!ndl[0])
		return ((char *)hs);
	i = 0;
	while (i < len && hs[i])
	{
		j = 0;
		while (i + j < len && ndl[j] && hs[i + j] == ndl[j])
			j++;
		if (!ndl[j])
			return (&((char *)hs)[i]);
		i++;
	}
	return (NULL);
}
