/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 17:55:43 by bnage             #+#    #+#             */
/*   Updated: 2018/12/08 20:48:55 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char			*x;
	unsigned int	i;
	size_t			len;

	if (s == NULL || f == NULL || *f == NULL)
		return (NULL);
	len = ft_strlen(s);
	x = (char *)malloc(len + 1);
	if (x == NULL)
		return (NULL);
	x[len] = '\0';
	i = 0;
	while (s[i])
	{
		x[i] = f(i, s[i]);
		i++;
	}
	return (x);
}
