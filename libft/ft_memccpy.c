/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 18:38:43 by bnage             #+#    #+#             */
/*   Updated: 2018/12/06 19:39:57 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t i;

	i = 0;
	while (i < n)
	{
		((t_uc *)dst)[i] = ((t_uc *)src)[i];
		i++;
		if (((t_uc *)src)[i - 1] == (t_uc)c)
			return (&(((t_uc *)dst)[i]));
	}
	return (NULL);
}
