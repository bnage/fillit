/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 20:49:57 by bnage             #+#    #+#             */
/*   Updated: 2018/12/08 21:28:43 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list *x;

	if (alst == NULL || del == NULL || *del == NULL || *alst == NULL)
		return ;
	while (*alst)
	{
		x = (*alst)->next;
		ft_lstdelone(alst, del);
		*alst = x;
	}
}
