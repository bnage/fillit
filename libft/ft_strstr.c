/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 21:46:00 by bnage             #+#    #+#             */
/*   Updated: 2018/12/08 20:30:23 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char	*ft_strstr(const char *hs, const char *ndl)
{
	size_t i;
	size_t j;

	i = 0;
	if (!ndl[0])
		return ((char *)hs);
	while (hs[i])
	{
		j = 0;
		while (ndl[j] && hs[i + j] == ndl[j])
			j++;
		if (!ndl[j])
			return (&(((char *)hs)[i]));
		i++;
	}
	return (NULL);
}
