/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_to_str.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/09 17:27:21 by bnage             #+#    #+#             */
/*   Updated: 2018/12/11 21:03:30 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static size_t	strlensum(t_list *lst, size_t ended)
{
	size_t len;

	len = 0;
	while (lst)
	{
		len += lst->content_size - (lst->content_size == 0 ? 0 : ended);
		lst = lst->next;
	}
	return (len);
}

static void		cpy(t_list *lst, char **ans, size_t en)
{
	size_t len;

	len = 0;
	while (lst)
	{
		if (lst->content_size == 0 || lst->content_size < en)
		{
			lst = lst->next;
			continue;
		}
		ft_memcpy((void *)&((*ans)[len]), lst->content, lst->content_size - en);
		len += lst->content_size - en;
		lst = lst->next;
	}
	(*ans)[len] = '\0';
}

char			*ft_lst_to_str(t_list *lst, size_t ended)
{
	size_t	len;
	char	*ans;

	if (lst == NULL)
		return (NULL);
	len = 0;
	if (ended != 0 && ended != 1)
		ended = 1;
	len = strlensum(lst, ended);
	ans = (char *)malloc(len + 1);
	if (ans == NULL)
		return (NULL);
	cpy(lst, &ans, ended);
	return (ans);
}
