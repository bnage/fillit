/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 20:34:21 by bnage             #+#    #+#             */
/*   Updated: 2018/12/08 21:25:10 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*x;
	void	*cnt;

	x = (t_list *)malloc(sizeof(t_list));
	if (x == NULL)
		return (NULL);
	if (content == NULL)
	{
		x->content = NULL;
		x->content_size = 0;
		x->next = NULL;
		return (x);
	}
	cnt = (void *)malloc(content_size);
	if (cnt == NULL)
	{
		free(x);
		return (NULL);
	}
	ft_memcpy(cnt, content, content_size);
	x->content = cnt;
	x->content_size = content_size;
	x->next = NULL;
	return (x);
}
