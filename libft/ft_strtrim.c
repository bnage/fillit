/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 15:47:05 by bnage             #+#    #+#             */
/*   Updated: 2018/12/08 21:13:06 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int		is_space(char c)
{
	if (c == ' ' || c == '\n' || c == '\t')
		return (1);
	else
		return (0);
}

char			*ft_strtrim(char const *s)
{
	size_t len;
	size_t sps;

	if (s == NULL)
		return (NULL);
	len = ft_strlen(s);
	sps = 0;
	while (is_space(s[sps]))
		sps++;
	if (sps == len)
		return (ft_strnew(0));
	while (is_space(s[len - 1]))
		len--;
	return (ft_strsub(s, sps, len - sps));
}
