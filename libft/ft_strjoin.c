/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 18:31:16 by bnage             #+#    #+#             */
/*   Updated: 2018/12/06 22:41:53 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*x;
	size_t	i;
	size_t	l;
	size_t	l2;

	if (s1 == NULL || s2 == NULL)
		return (NULL);
	l = ft_strlen(s1);
	l2 = ft_strlen(s2);
	x = (char *)malloc(l + l2 + 1);
	if (x == NULL)
		return (NULL);
	x[l + l2] = '\0';
	i = 0;
	while (s1[i])
	{
		x[i] = s1[i];
		i++;
	}
	while (s2[i - l])
	{
		x[i] = s2[i - l];
		i++;
	}
	return (x);
}
