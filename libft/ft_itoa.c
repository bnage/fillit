/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 16:57:41 by bnage             #+#    #+#             */
/*   Updated: 2018/12/08 21:17:26 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static size_t	ft_itoa0(int n, char *x)
{
	size_t i;

	i = 0;
	while (n)
	{
		x[11 - i] = (n < 0 ? -(n % 10) : n % 10) + '0';
		n = n < 0 ? -(n / 10) : n / 10;
		i++;
	}
	return (i);
}

char			*ft_itoa(int n)
{
	char	x[12];
	size_t	size;
	size_t	i;
	char	*ans;

	ft_memset(x, 'r', 12);
	if (n <= 0)
		x[0] = n == 0 ? '0' : '-';
	i = ft_itoa0(n, x);
	size = i;
	if (x[0] != 'r')
		size = x[0] == '0' ? 1 : size + 1;
	ans = (char *)malloc(size + 1);
	if (ans == NULL)
		return (NULL);
	ans[size] = '\0';
	if (x[0] != 'r')
	{
		ans[0] = x[0];
		if (x[0] == '-')
			ft_memcpy(&(ans[1]), &(x[12 - i]), i);
	}
	else
		ft_memcpy(ans, &(x[12 - i]), i);
	return (ans);
}
