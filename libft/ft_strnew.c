/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/04 17:57:28 by bnage             #+#    #+#             */
/*   Updated: 2018/12/08 22:09:31 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strnew(size_t size)
{
	char *x;

	if (size > size + 1)
		return (NULL);
	x = (char *)malloc(size + 1);
	if (x == NULL)
		return (NULL);
	ft_memset((void *)x, '\0', size + 1);
	return (x);
}
