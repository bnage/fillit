/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 18:15:35 by bnage             #+#    #+#             */
/*   Updated: 2018/12/08 21:56:06 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*x;
	size_t	i;

	if (s == NULL || len > len + 1)
		return (NULL);
	x = (char *)malloc(len + 1);
	if (x == NULL)
		return (NULL);
	x[len] = '\0';
	i = 0;
	while (i < len)
	{
		x[i] = s[start + i];
		i++;
	}
	return (x);
}
