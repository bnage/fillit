/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 19:59:26 by bnage             #+#    #+#             */
/*   Updated: 2018/12/06 22:23:15 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strdup(const char *s1)
{
	size_t	i;
	char	*ans;

	i = ft_strlen(s1);
	ans = (char *)malloc(i + 1);
	if (ans == NULL)
		return (NULL);
	ans[i] = '\0';
	i = 0;
	while (s1[i])
	{
		ans[i] = s1[i];
		i++;
	}
	return (ans);
}
