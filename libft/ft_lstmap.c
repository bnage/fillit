/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <bnage@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 21:02:16 by bnage             #+#    #+#             */
/*   Updated: 2019/01/24 18:08:02 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static void	del(void *cnt, size_t cnt_s)
{
	free(cnt);
	cnt_s = cnt_s + 1;
}

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *x;
	t_list *end;

	if (lst == NULL || f == NULL || *f == NULL)
		return (NULL);
	x = f(lst);
	if (x == NULL)
		return (NULL);
	end = x;
	lst = lst->next;
	while (lst)
	{
		end->next = f(lst);
		if (end->next == NULL)
		{
			ft_lstdel(&x, &del);
			return (NULL);
		}
		end = end->next;
		lst = lst->next;
	}
	end->next = NULL;
	return (x);
}
