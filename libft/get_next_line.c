/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <bnage@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/09 14:12:45 by bnage             #+#    #+#             */
/*   Updated: 2018/12/13 21:12:44 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "libft.h"

static t_list	*push_back(t_list **lst, t_list **end, t_buffer *buf, size_t i)
{
	t_list	*el;

	el = ft_lstnew((void *)&((buf->buf)[buf->i]), i - buf->i);
	ft_lstpb(end, el);
	if (!(*lst))
		*lst = *end;
	return (el);
}

static int		read_line(t_list **lst, t_list **end, t_buffer *buf)
{
	size_t	i;
	t_list	*el;

	i = buf->i;
	while (i < BUFF_SIZE)
	{
		if ((buf->buf)[i] == '\n' || !((buf->buf)[i]))
		{
			el = push_back(lst, end, buf, i);
			buf->i = i + 1;
			if (i + 1 == BUFF_SIZE || !(buf->buf)[i] || !(buf->buf)[i + 1])
				buf->i = 0;
			return (el == NULL ? -1 : 1);
		}
		i++;
	}
	el = push_back(lst, end, buf, i);
	buf->i = 0;
	return (el == NULL ? -1 : 0);
}

static int		return_line(char **line, t_list *lst, int j)
{
	t_list *x;

	*line = ft_lst_to_str(lst, 0);
	while (lst)
	{
		x = (lst)->next;
		free(lst->content);
		free(lst);
		lst = x;
	}
	return (j);
}

static int		get_next_line0(t_buffer *buf, const int fd, char **line)
{
	int			i;
	int			j;
	t_list		*lst;
	t_list		*end;

	lst = NULL;
	end = NULL;
	if (buf->i && (j = read_line(&lst, &end, buf)))
		return (return_line(line, lst, j));
	while ((i = read(fd, buf->buf, BUFF_SIZE)) && i > 0)
	{
		if (i != BUFF_SIZE)
			(buf->buf)[i] = '\0';
		if ((j = read_line(&lst, &end, buf)))
			return (return_line(line, lst, j));
	}
	if (i < 0)
		return (-1);
	buf->i = 0;
	if (lst == NULL)
		return (0);
	return (return_line(line, lst, 1));
}

int				get_next_line(const int fd, char **line)
{
	static t_buffer	*buf[MAX];
	int				i;

	if (fd < 0 || line == NULL || fd >= MAX)
		return (-1);
	if (buf[fd] == NULL)
	{
		buf[fd] = (t_buffer *)malloc(sizeof(t_buffer));
		if (!buf[fd])
			return (-1);
		buf[fd]->buf = (char *)malloc(BUFF_SIZE);
		if (!(buf[fd]->buf))
		{
			ft_memdel((void **)&(buf[fd]));
			return (-1);
		}
		buf[fd]->i = 0;
	}
	i = get_next_line0(buf[fd], fd, line);
	if (i < 1)
	{
		ft_memdel((void **)&(buf[fd]->buf));
		ft_memdel((void **)&(buf[fd]));
	}
	return (i);
}
