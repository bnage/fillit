/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 18:01:04 by bnage             #+#    #+#             */
/*   Updated: 2018/12/07 18:04:19 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t i;

	i = 0;
	while (i < n && s1[i] && ((t_uc *)s1)[i] == ((t_uc *)s2)[i])
		i++;
	return (i == n ? 0 : ((t_uc *)s1)[i] - ((t_uc *)s2)[i]);
}
