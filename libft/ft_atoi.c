/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 18:05:41 by bnage             #+#    #+#             */
/*   Updated: 2018/12/08 18:41:17 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_atoi(const char *str)
{
	size_t				i;
	unsigned long long	ans;
	int					minus;
	size_t				count;

	count = 0;
	i = 0;
	ans = 0;
	minus = 1;
	while ((str[i] >= 9 && str[i] <= 13) || str[i] == 32)
		i++;
	if (str[i] == '-' || str[i] == '+')
		minus = str[i++] == '-' ? -1 : 1;
	while (ft_isdigit(str[i]))
	{
		ans *= 10;
		ans += (str[i] - '0');
		count++;
		i++;
	}
	if (count > 19 || ans >= 9223372036854775808uLL)
		return ((minus == 1) ? -1 : 0);
	if (ans == 2147483648uLL && minus == -1)
		return (-2147483648);
	return ((int)ans * minus);
}
