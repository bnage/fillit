/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bnage <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 16:19:00 by bnage             #+#    #+#             */
/*   Updated: 2018/12/08 21:15:24 by bnage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static size_t	count(char const *s, char c)
{
	size_t size;
	size_t last;
	size_t i;

	size = 0;
	last = 0;
	i = 0;
	while (s[i])
	{
		if (s[i] == c && last == 1)
			last = 0;
		else if (s[i] != c && last == 0)
		{
			size++;
			last = 1;
		}
		i++;
	}
	return (size);
}

static size_t	len(char const *s, size_t i, char c)
{
	size_t length;

	length = 0;
	while (s[length + i] && s[length + i] != c)
		length++;
	return (length);
}

static void		ft_free(char ***ans, size_t size)
{
	size_t i;

	i = 0;
	while (i < size)
		free((*ans)[i++]);
	free(*ans);
	*ans = NULL;
}

static void		ft_strsplit0(char ***ans, char const *s, char c)
{
	size_t i;
	size_t j;
	size_t length;

	i = 0;
	j = 0;
	while (s[i])
	{
		while (s[i] == c)
			i++;
		if (!s[i])
			break ;
		length = len(s, i, c);
		(*ans)[j++] = ft_strsub(s, i, length);
		if ((*ans)[j - 1] == NULL)
		{
			ft_free(ans, j);
			return ;
		}
		i += length;
	}
}

char			**ft_strsplit(char const *s, char c)
{
	size_t	size;
	char	**ans;

	if (s == NULL)
		return (NULL);
	size = count(s, c);
	ans = (char **)malloc(sizeof(char *) * (size + 1));
	if (ans == NULL)
		return (NULL);
	ans[size] = 0;
	ft_strsplit0(&ans, s, c);
	return (ans);
}
